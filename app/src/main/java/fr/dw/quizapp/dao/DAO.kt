package fr.dw.quizapp.dao

import android.content.Context
import android.database.Cursor
import fr.dw.quizapp.job.Answer
import fr.dw.quizapp.job.Question
import fr.dw.quizapp.job.Theme
import java.util.*
import kotlin.collections.ArrayList


class DAO(context: Context) {

    private var myBaseSQLite = MySQLite.getInstance(context)
    private var bdd = myBaseSQLite.readableDatabase

    fun getQuestions(theme: Theme, numberOfQuestions:Int): Stack<Question> {

        val stackQuestions = Stack<Question>()
        var cursor: Cursor? = null
        val query =
            "SELECT * from QUESTION as Q INNER JOIN THEME as T on T.id = themeID WHERE themeID = " + theme.id + " ORDER BY RANDOM() LIMIT $numberOfQuestions"

        try {
            cursor = bdd.rawQuery(query, null)
            while (cursor.moveToNext()) {
                var questionGenerated = Question(0, "", 0)
                questionGenerated =
                    Question(cursor.getInt(0), cursor.getString(1), cursor.getInt(2))
                questionGenerated.addAnswers(getAnswersOfQuestion(questionGenerated.id))
                stackQuestions.push(questionGenerated)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
        }
        return stackQuestions
    }

    private fun getAnswersOfQuestion(questionID: Int): ArrayList<Answer> {
        val answers = ArrayList<Answer>()
        var cursor: Cursor? = null
        val query =
            "SELECT * from ANSWER WHERE questionID = $questionID ORDER BY RANDOM()"

        try {
            cursor = bdd.rawQuery(query, null)
            while (cursor.moveToNext()) {
                answers.add(
                    Answer(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getInt(2) == 1,
                        cursor.getInt(3)
                    )
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            cursor?.close()
        }

        return answers
    }

    fun getAllTheme(): ArrayList<Theme> {
        val themes = ArrayList<Theme>()
        var cursor: Cursor? = null

        try {
            cursor = bdd.rawQuery("SELECT * FROM THEME", null)
            while (cursor.moveToNext()) {
                themes.add(cursorToTheme(cursor))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return themes
        } finally {
            cursor?.close()
        }
        return themes
    }

    private fun cursorToTheme(cursor: Cursor): Theme {
        return Theme(
            cursor.getInt(0),
            cursor.getString(2),
            cursor.getString(1),
            cursor.getString(3)
        )
    }
}
