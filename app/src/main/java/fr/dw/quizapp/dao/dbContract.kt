package fr.dw.quizapp.dao

import android.provider.BaseColumns


object dbContract {

    class ThemeEntry : BaseColumns {
        companion object {
            val TABLE_NAME = "THEME"
            val COLUMN_ID = "id"
            val COLUMN_LIBELLE = "libelle"
            val COLUMN_ICONE = "icon"
        }

    }
}
