package fr.dw.quizapp.job

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Theme(var id: Int, var icon: String, var libelle: String, var description: String):Parcelable