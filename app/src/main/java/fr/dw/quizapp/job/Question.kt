package fr.dw.quizapp.job

data class Question(var id: Int, var libelle: String, var themeID: Int) {

    var answers = ArrayList<Answer>()

    fun addAnswers(answer: ArrayList<Answer>): ArrayList<Answer> {
        answers.addAll(answer)
        return answers
    }

    override fun toString(): String {
        return libelle
    }

}