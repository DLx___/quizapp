package fr.dw.quizapp.job

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Answer(var id:Int, var libelle:String, var isCorrect:Boolean, var questionID:Int):Parcelable {

    override fun toString(): String {
        return libelle
    }
}