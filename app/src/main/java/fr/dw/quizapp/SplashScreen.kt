package fr.dw.quizapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val thread = object : Thread() {
            override fun run() {
                try {
                    sleep(2500)
                    val myIntent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(myIntent)
                    finish()

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
        thread.start()
    }
}
