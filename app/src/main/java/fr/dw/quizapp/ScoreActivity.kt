package fr.dw.quizapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.final_score.*

class ScoreActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.final_score)
    }

    override fun onResume() {
        supportActionBar?.hide()
        super.onResume()
        val numberOfQuestion = GameActivity.numberOfQuestionGenerated
        val scoreBuilder = StringBuilder()
        val score = intent.getIntExtra("finalScore", 0)
        scoreBuilder.append("Score final :").append(" ").append("$score/$numberOfQuestion")
        textViewFinalScore.text = scoreBuilder
        btnBackToTheme.setOnClickListener { this.finish() }
    }
}