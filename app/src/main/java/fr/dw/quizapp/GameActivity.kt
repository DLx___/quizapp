package fr.dw.quizapp

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isInvisible
import fr.dw.quizapp.dao.DAO
import fr.dw.quizapp.job.Answer
import fr.dw.quizapp.job.Question
import fr.dw.quizapp.job.Theme
import kotlinx.android.synthetic.main.game.*
import java.util.*
import kotlin.collections.ArrayList

class GameActivity : AppCompatActivity() {

    private var theme: Theme? = null
    private val dao = DAO(this)
    private var questionStack: Stack<Question>? = null
    private var questions = Question(0, "", 0)
    private var score: Int = 0

    companion object {
        var numberOfQuestionGenerated = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game)
    }

    override fun onResume() {
        supportActionBar?.hide()
        super.onResume()
        theme = intent.getParcelableExtra("theme")
        checkIfIsHarryPotterTheme()
        questionStack = dao.getQuestions(theme!!, numberOfQuestionGenerated)

        btnNext.setOnClickListener {
            btnNextClicked()
        }
        btnQuit.setOnClickListener {
            btnQuitClicked()
        }
        refreshScore()
        generateQuestion()
    }

    private fun checkIfIsHarryPotterTheme() {
        if (theme!!.id == 4) {
            numberOfQuestionGenerated = 20
        } else {
            numberOfQuestionGenerated = 5
        }
    }

    private fun btnQuitClicked() {
        this.finish()
    }

    private fun btnNextClicked() {
        if (questionStack!!.size > 0) {
            generateQuestion()
        } else {
            showFinalScore(score)
        }
    }

    private fun generateQuestion() {
        questions = questionStack!!.pop()
        linearBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
        questionID.text = questions.libelle
        textViewAnswer.text = ""
        showButton(true)

        listViewAnswers.isEnabled = true
        listViewAnswers.adapter =
            ArrayAdapter<Answer>(this, android.R.layout.simple_list_item_1, questions.answers)
        listViewAnswers.setOnItemClickListener { parent, _, position, _ ->
            parent.isEnabled = false
            checkAnswer(parent.getItemAtPosition(position) as Answer)
        }
    }

    private fun checkAnswer(answer: Answer) {
        //linearBackground.resources.getColor(R.color.red)
        val answerBuilder = StringBuilder()
        val answerOfQuestion = ArrayList<Answer>(questions.answers)
        var correctAnswer: String? = ""

        for (myAnswer in answerOfQuestion) {
            if (myAnswer.isCorrect) {
                correctAnswer = myAnswer.libelle
            }
        }

        answerBuilder.append("La réponse était : ").append(" ").append(correctAnswer)

        when (answer.isCorrect) {
            true -> {
                linearBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.green))
                score++
                refreshScore()
            }
            false -> {
                linearBackground.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
                textViewAnswer.text = answerBuilder
            }
        }
        showButton(false)
    }

    private fun showButton(state: Boolean) {

        when (state) {
            true -> {
                btnNext.isInvisible = true
                btnQuit.isInvisible = true
            }
            false -> {
                btnNext.isInvisible = false
                btnQuit.isInvisible = false
            }
        }
    }


    private fun refreshScore() {
        val scoreBuilder = StringBuilder()
        scoreBuilder.append(textViewScore.resources.getText(R.string.score)).append(" ")
            .append(score)
        textViewScore.text = scoreBuilder.toString()
    }

    private fun showFinalScore(score: Int) {
        val myIntent = Intent(this, ScoreActivity::class.java)
        myIntent.putExtra("finalScore", score)
        startActivityForResult(myIntent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        this.finish()
    }
}