package fr.dw.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import fr.dw.quizapp.dao.DAO
import fr.dw.quizapp.job.Theme
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var themeSelected = Theme(0, "", "", "")

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val themes = themeGenerator()
        val myAdapter = ThemeAdapter(this, R.layout.row, themes)

        ListView.adapter = myAdapter
        ListView.setOnItemClickListener { parent, _, position, _ ->
            themeSelected = parent.getItemAtPosition(position) as Theme
            launchGame(themeSelected)
        }
    }

    private fun themeGenerator(): ArrayList<Theme> {
        val dao = DAO(this)
        return ArrayList(dao.getAllTheme())
    }

    private fun launchGame(theme: Theme) {
        val myIntent = Intent(this, GameActivity::class.java)
        myIntent.putExtra("theme", theme)
        startActivity(myIntent)
    }
}

