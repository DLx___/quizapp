package fr.dw.quizapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import fr.dw.quizapp.job.Theme

class ThemeAdapter(context: Context, resource: Int, theme: ArrayList<Theme>) :
    ArrayAdapter<Theme>(context, resource, theme) {

    private var themes = theme

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var row = convertView

        if (convertView == null) {
            val layoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            row = layoutInflater.inflate(R.layout.row, parent, false)
            val theme = themes[position]
            val libelle = row.findViewById<TextView>(R.id.TextViewLibelle)
            val icon = row.findViewById<ImageView>(R.id.ImageView)
            val description = row.findViewById<TextView>(R.id.TextViewDescription)

            val iconID = row.resources.getIdentifier(theme.icon, "drawable", "fr.dw.quizapp")
            val drawableIcon = ContextCompat.getDrawable(context, iconID)

            icon.setImageDrawable(drawableIcon)
            libelle.text = theme.libelle
            description.text = theme.description
        }
        return row!!
    }
}