CREATE TABLE THEME ( 
	id intEGER PRIMARY KEY AUTOINCREMENT,
  	libelle VARCHAR(100),
  	icon VARCHAR(100)
, description VARCHAR(100));

CREATE TABLE QUESTION (
	id intEGER PRIMARY KEY AUTOINCREMENT,
  	libelle VARCHAR(100),
  	themeID int,
  	FOREIGN KEY (themeID) REFERENCES theme(id)
);
 
CREATE TABLE ANSWER (
	id intEGER PRIMARY KEY AUTOINCREMENT,
  	libelle VARCHAR(100),
   	isCorrect boolean,
  	questionID int,
  	FOREIGN KEY (questionID) REFERENCES QUESTION(id)
);

INSERT INTO THEME (libelle,icon,description) VALUES (
  'Mathématique',
  'maths',
  '1 + 1 = ?'
); 
 
INSERT INTO THEME (libelle,icon,description) VALUES (
  'Géographie',
  'geo',
  'Que sais-tu vraiment sur la Terre...'
); 

INSERT INTO THEME (libelle,icon,description) VALUES (
  'Culture cinématographique',
  'cinema',
  'Es-tu un vrai fan de cinéma ?'
);

INSERT INTO THEME (libelle,icon,description) VALUES (
  'Harry-Potter',
  'harry-potter',
  'Es-tu un vrai fan d''Harry-Potter ?'
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quel nombre n''est pas un nombre premier ?',
  '1'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'37',false,1
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'61',false,1
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'63',true,1
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'89',false,1
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quelle lettre suit la lettre gamma dans l''alphabet grec ?',
  '1'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'kappa',false,2
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'iota',false,2
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'lambda',false,2
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'delta',true,2
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Le centre de gravité d''un triangle est l''intersection ?',
  '1'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'des trois hauteurs',false,3
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'des trois médiatrices',false,3
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'des trois bissectrices',false,3
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'des trois médianes',true,3
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Dans sinus (x) , x représente ?',
  '1'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'un angle',true,4
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'une surface',false,4
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'un pourcentage',false,4
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'une distance',false,4
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Le cosinus d''un angle varie ?',
  '1'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'entre -PI et +PI',false,5
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'entre 0 et PI',false,5
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'entre -1 et +1',true,5
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'entre 0 et +1',false,5
);



INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quelle est la capitale de la Nouvelle Zélande ?',
  '2'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Port Moresby',false,6
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Port Louis',false,6
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Port d''Espagne',false,6
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Wellington',true,6
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Vous vous promenez le long de la côte des Abruses, quelle mer voyez-vous ?',
  '2'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Ocean Altlantique',false,7
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Mer Egée',false,7
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Mer Adriatique',true,7
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Mer Tirrheniene',false,7
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'La ville de Milan se trouve dans quelle province ?',
  '2'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Lombardie',true,8
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Le Frioule',false,8
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Les Abruzzes',false,8
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'La Campanie',false,8
);

INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Parmis les critères qui permettent de définir la pauvreté, quel est l''intrus ?',
  '2'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'L''accès à la santé',false,9
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'L''accès à l''éducation',false,9
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Les conditions de vies faciles',true,9
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'L''accès à un logement',false,9
);

INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quel est le nombre d''être humain sur la Terre (en 2019) ?',
  '2'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'7,6 millions',false,10
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'76 millions',false,10
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'7,6 milliards',true,10
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'76 milliards',false,10
);



INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Comment s''appelle l''actrice qui joue le rôle de "Eleven" dans la série Stranger Things ? ',
  '3'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Milie Bobby Brown',true,11
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Finn Wolfhard',false,11
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Gaten Matarazzo',false,11
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Autre',false,11
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quel est l''horcruxe détruit dans le tome 2 dans "Harry Potter et la chambre des secrets" ?',
  '3'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Le médaillon',false,12
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Le journal',true,12
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Nagini',false,12
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'La bague',false,12
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Comment est nurnommé le personnage de Morgan Freeman dans "Les Evadés" ?',
  '3'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Blue',false,13
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Green',false,13
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Green',false,13
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Red',true,13
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'A quelle guerre a participé Clint Eastwood dans le film "Gran Torino" ?',
  '3'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Guerre de Corée',true,14
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Guerre d''Algérie',false,14
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Guerre du Vietnam',false,14
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Guerre du Liban',false,14
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quel grade est Ludovic Cruchot dans la saga des "Gendarme de St-Tropez" ?',
  '3'
); 
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Adjudant',false,15
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Maréchal des logis-chef',true,15
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Lieutenant',false,15
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Général',false,15
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Qu''enseignait Albus Dumbledore avant de devenir le directeur de Poudlard ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'L''histoire de la magie',false,16
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'La métamorphose',true,16
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'La défense contre les forces du mal',false,16
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Les sortilèges',false,16
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Le respecté directeur de Poudlard, Albus Dumbledore, prétend avoir une cicatrice au dessus du genou gauche. A quoi ressemble-t-elle, selon lui ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est "un plan parfait du métro de Londres"',true,17
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est "un portrait craché de la Joconde"',false,17
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est "une carte précise de l''Écosse"',false,17
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est "la magnifique constellation du Phénix"',false,17
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
    'En quelle année sont morts les parents de Harry Potter ?',
    '4'
  );
  INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
  	'En 1980',false,18
  );
  INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
  	'En 1981',true,18
  );
  INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
  	'En 1982',false,18
  );
  INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
  	'En 1983',false,18
  );

INSERT INTO QUESTION (libelle,themeID) VALUES (
  'A quelle variété appartient Norbert, le dragon élevé en secret par Hagrid ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est un dangereux Magyar à pointes',false,19
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est un Norvégien à crête, très rare et agressif',true,19
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est un Vert gallois, qui aime la musique',false,19
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'C''est un Opaloeil des antipodes, certainement l''un des plus beaux dragons du monde',false,19
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Dans "Harry Potter à l''école des sorciers", de quelle créature Voldemort doit-il boire le sang pour survivre ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Un centaure',false,20
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Un phénix',false,20
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Une licorne',true,20
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Un hippogriffe',false,20
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Lequel de ces mots de passe n''a jamais permis d''ouvrir le bureau d''Albus Dumbledore ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Fizwizbiz',false,21
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Nid de cafard',false,21
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Eclairs au caramel',false,21
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Chocogrenouille',true,21
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quelle est la note la plus basse qu''un élève de Poudlard puisse obtenir aux examens de sorcellerie Buse, Aspic et Emeu ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'M pour Moldus',false,22
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'T pour Troll',true,22
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'P pour Piètre',false,22
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'D pour Désolant',false,22
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Laquelle de ces propositions n''est pas un Horcruxe dans lequel Voldemort a enfermé une partie de son âme ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Le journal intime de Tom Jedusor',false,23
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Le médaillon de Salazar Serpentard',false,23
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Harry Potter',false,23
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'L''épée de Godric Gryffondor',true,23
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quelle est la devise de Poudlard ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Nolite te bastardes carborundorum"',false,24
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Hic sunt dracones"',false,24
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Draco dormiens nunquam titillandus"',true,24
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Citius, altius, fortius"',false,24
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Pourquoi Hermione a-t-elle l''idée de créer l''Armée de Dumbledore ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Pour que les élèves apprennent à se battre, après l''arrêt des cours pratiques de défense contre les forces du mal',true,25
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Pour venger la mort de Dumbledore, tué par Severus Rogue',false,25
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Pour montrer à tous qu''elle est l''élève la plus intelligente de Poudlard',false,25
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Pour se rebeller contre Dolores Ombrage, qui a remplacé Dumbledore à la tête de Poudlard',false,25
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Vous avez entre les mains la carte du Maraudeur. Mais elle ressemble pour l''instant à un vieux parchemin sans intérêt. Que devez vous dire pour l''activer ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Je jure solennellement que mes intentions sont mauvaises"',true,26
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Je jure sur la tête de mon hibou que je suis Albus Dumbledore"',false,26
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Je jure que je n''ai pas l''intention de faire de bêtise"',false,26
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Je jure solennellement que je ne suis pas Severus Rogue"',false,26
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quels sont les derniers mots de Severus Rogue, dans "Les Reliques de la mort" ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Pas Harry ! Ayez pitié !"',false,27
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Regardez… moi"',true,27
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Tu peux faire mieux que ça !"',false,27
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Avada Kedavra"',false,27
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Qu''arrive-t-il à George Weasley lors de la "bataille des sept Potter" ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Il meurt',false,28
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Il perd un doigt',false,28
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Il perd l''oreille gauche',true,28
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Il perd l''oreille droite',false,28
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Qui sont les quatre sorciers fondateurs de Poudlard ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Roselia Serdaigle, Gordius Gryffondor, Holga Poufsouffle, Salazar Serpentard',false,29
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Rowena Serdaigle, Godric Gryffondor, Helga Poufsouffle, Salazar Serpentard',true,29
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Rowena Serdaigle, Gidric Gryffondor, Hilge Poufsouffle, Saladin Serpentard',false,29
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Ramona Serdaigle, Godric Gryffondor, Helga Boursouffle, Sandor Serpentard',false,29
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Le jour de son 11e anniversaire, Harry Potter apprend qu''il a un coffre à son nom à la banque Gringotts, quel est le numéro de son coffre ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'713',false,30
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'711',false,30
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'687',true,30
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'207',false,30
);



INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Quel numéro faut-il taper dans la cabine téléphonique désaffectée de Londres pour entrer au ministère de la Magie ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'6-2-4-4-2',true,31
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'3-8-3-4-3',false,31
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'2-5-2-8-7',false,31
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'4-2-7-7-9',false,31
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Lequel des quatre maraudeurs n’est pas un Animagus, c''est-à-dire un sorcier capable de se métamorphoser en animal ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'James Potter',false,32
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Sirius Black',false,32
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Remus Lupin',true,32
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Peter Pettigrow',false,32
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Combien y a-t-il d''escaliers magiques à Poudlard ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'132',false,33
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'142',true,33
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'152',false,33
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'162',false,33
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'En 1994 se déroule la 422e Coupe du monde de quidditch. En finale, la Bulgarie emmenée par l''attrapeur Viktor Krum affronte l''Irlande d''Aidan Lynch. Le match est serré et violent. Quel est le score final ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'L''Irlande l''emporte, 170 à 160',true,34
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'La Bulgarie gagne 160 à 150',false,34
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Egalité : 170 à 170, c''est du jamais-vu !',false,34
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'On ne connaît pas le résultat, des Mangemorts ont interrompu la rencontre',false,34
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
    'Quels sont les derniers mots de Severus Rogue, dans "Les Reliques de la mort" ?' ,
     '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Pas Harry ! Ayez pitié !"',false,35
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Regardez… moi"',true,35
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Tu peux faire mieux que ça !"',false,35
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'"Avada Kedavra"',false,35
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Harry Potter a grandi chez son oncle et sa tante après la mort de ses parents. Mais comment s’appellent-ils ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Arthur et Molly Weasley',false,36
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Petunia et Vernon Dursley',true,36
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Wendell et Monica Granger',false,36
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Dans le premier tome, quel personnage vient chercher Harry chez les Dursley ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Albus Dumbledore',false,37
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Rubeus Hagrid',true,37
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Hermione Granger',false,37
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Les Moldus sont :',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Les mauvais élèves à Poudlard',false,38
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Les friandises préférées des magiciens',false,38
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Les personnes sans pouvoirs magiques',true,38
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Au début de leur scolarité, les apprentis sorciers choisissent un animal de compagnie. Quel est celui de Ron Weasley ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Un chat',false,39
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Une chouette',false,39
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Un rat',true,39
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Comment accède-t-on au chemin de Traverse, la rue commerçante où les élèves de Poudlard font leurs achats ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'En ouvrant le mur d’un pub invisible aux yeux des Moldust',true,40
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'En franchissant le mur du son à l’aide d’un balai volant',false,40
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'En téléphonant depuis une cabine téléphonique',false,40
);


INSERT INTO QUESTION (libelle,themeID) VALUES (
  'Comment s''appelle le fantôme qui représente la maison Serpentard ?',
  '4'
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Le Baron sanglant',true,41
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'Nick Quasi-Sans-Tête',false,41
);
INSERT INTO ANSWER (libelle,isCorrect,questionID) VALUES (
	'La Dame grise',false,41
);












